/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined(_GNU_SOURCE)
#define _GNU_SOURCE
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <signal.h>
#include <execinfo.h>
#include <stdarg.h>
#include <ctype.h>
#include <stdint.h>
#include <shadow.h>


#define DEMON_NAME "confrst"
#define PID_FILE "/var/run/" DEMON_NAME ".pid"

#define BUTTON_PUSHED_VALUE '0'
#define MAX_BUF 64
#define STRING_SIZE 100

struct sigaction act;
int fd_button_value;
int fd_led_trigger;

char default_value_led_config[STRING_SIZE] = "";

void writeLog(const char* msg, ...) {
  va_list a_list;
  char buffer[BUFSIZ];

  openlog(DEMON_NAME, LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);

  va_start(a_list, msg);
  vsnprintf(buffer, sizeof (buffer), msg, a_list);
  va_end(a_list);

  syslog(LOG_INFO, buffer);

  closelog();
}

void setPidFile(char* filename) {
  FILE *f;

  f = fopen(filename, "w+");
  if (f == NULL) {
    writeLog("can't open %s, error: %s (%d)\n", filename, strerror(errno), errno);
    exit(EXIT_FAILURE);
  }
  if (f) {
    fprintf(f, "%u", getpid());
    fclose(f);
  }
}

void sighandler(int signum, siginfo_t *info, void *ptr) {
  writeLog("received signal %s(%d) from process %lu\n", strsignal(signum), signum, (unsigned long) info->si_pid);

  if (write(fd_led_trigger, default_value_led_config, strlen(default_value_led_config)) < 0) {
    writeLog("can't write to led file, error: %s (%d)\n", strerror(errno), errno);
  }
  unlink(PID_FILE);
  close(fd_button_value);
  close(fd_led_trigger);

  writeLog("stoped demon");
  if (signum == SIGSTOP) {
    exit(SIGINT);
  } else {
    exit(signum);
  }
}

int main(int argc, char **argv) {
  pid_t pid, sid;

  char path_to_button[STRING_SIZE];
  char path_to_led[STRING_SIZE];
  int button_hold_guard_time;
  char command_for_copy_config_files[STRING_SIZE];
  char default_root_password[STRING_SIZE];

  if (argc != 11) {
    printf("Usage: ./confrst -b path_to_button -t button_hold_guard_time -l path_to_led -c path_to_defauld_config_dir -p default_root_password\n");
    exit(EXIT_FAILURE);
  }

  int c;
  while ((c = getopt(argc, argv, "b:t:l:c:p:")) != -1) {
    switch (c) {
      case 'b':
        snprintf(path_to_button, sizeof(path_to_button), "%s", optarg);
        break;
      case 't':
        button_hold_guard_time = atoi(optarg);
        if (button_hold_guard_time == 0 && optarg[0] != '0') {
          printf("'-t' must be int\n");
          exit(EXIT_FAILURE);
        }
        break;
      case 'l':
        snprintf(path_to_led, sizeof(path_to_led), "%s", optarg);
        break;
      case 'c':
        snprintf(command_for_copy_config_files, sizeof(command_for_copy_config_files), "cp --backup --suffix=.confrst.`date +%%d_%%m_%%Y_%%T` -r %s/* /", optarg);
        break;
      case 'p':
        snprintf(default_root_password, sizeof(default_root_password), optarg);
        break;
      default:
        abort();
    }
  }

  pid = fork();
  if (pid < 0) {
    printf("start demon error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  } else if (pid > 0) {
    exit(EXIT_SUCCESS);
  }
  writeLog("starting demon");

  umask(0);

  sid = setsid();
  if (sid < 0) {
    writeLog("error while create a new SID for the child process");
    exit(EXIT_FAILURE);
  }

  if ((chdir("/")) < 0) {
    writeLog("error while change the current working directory");
    exit(EXIT_FAILURE);
  }

  int fd_log = open("/var/log/syslog", O_WRONLY | O_APPEND | O_CREAT, 0666);
  if (fd_log == -1) {
    writeLog("can't open log file, error: %s (%d)\n", strerror(errno), errno);
    exit(EXIT_FAILURE);
  }
  dup2(fd_log, STDOUT_FILENO);
  dup2(fd_log, STDERR_FILENO);
  close(fd_log);
  close(STDIN_FILENO);

  memset(&act, 0, sizeof (act));

  act.sa_sigaction = sighandler;
  act.sa_flags = SA_SIGINFO;

  sigaction(SIGTERM, &act, NULL);
  sigaction(SIGINT, &act, NULL);
  sigaction(SIGHUP, &act, NULL);
  sigaction(SIGSTOP, &act, NULL);

  sigaction(SIGFPE, &act, NULL);
  sigaction(SIGILL, &act, NULL);
  sigaction(SIGSEGV, &act, NULL);
  sigaction(SIGBUS, &act, NULL);

  setPidFile(PID_FILE);

  const char *edge = "falling";
  char path_to_button_edge[STRING_SIZE];
  snprintf(path_to_button_edge, sizeof(path_to_button_edge), "%s/edge", path_to_button);
  int fd_button_edge = open(path_to_button_edge, O_WRONLY);
  if (fd_button_edge == -1) {
    writeLog("can't open %s, error: %s (%d)\n", path_to_button_edge, strerror(errno), errno);
    exit(EXIT_FAILURE);
  }
  if (write(fd_button_edge, edge, strlen(edge)) < 0) {
    writeLog("can't write %s, error: %s (%d)\n", path_to_button_edge, strerror(errno), errno);
    exit(EXIT_FAILURE);
  }
  close(fd_button_edge);

  char path_to_button_value[STRING_SIZE];
  snprintf(path_to_button_value, sizeof(path_to_button_value), "%s/value", path_to_button);
  fd_button_value = open(path_to_button_value, O_RDONLY);
  if (fd_button_value == -1) {
    writeLog("can't open %s, error: %s (%d)\n", path_to_button_value, strerror(errno), errno);
    exit(EXIT_FAILURE);
  }

  char path_to_led_trigger[STRING_SIZE];
  char path_to_led_delay_on[STRING_SIZE];
  char path_to_led_delay_off[STRING_SIZE];

  snprintf(path_to_led_trigger, sizeof(path_to_led_trigger), "%s/trigger", path_to_led);
  snprintf(path_to_led_delay_on, sizeof(path_to_led_delay_on), "%s/delay_on", path_to_led);
  snprintf(path_to_led_delay_off, sizeof(path_to_led_delay_off), "%s/delay_off", path_to_led);
  fd_led_trigger = open(path_to_led_trigger, O_RDWR);
  if (fd_led_trigger == -1) {
    writeLog("can't open %s, error: %s (%d)\n", path_to_led_trigger, strerror(errno), errno);
    exit(EXIT_FAILURE);
  }

  //save default value
  char default_led_config[200] = "";

  ssize_t len = read(fd_led_trigger, default_led_config, sizeof(default_led_config));
  if (len < 0) {
    writeLog("can't read %s, error: %s (%d)\n", path_to_led_trigger, strerror(errno), errno);
    exit(EXIT_FAILURE);
  }

  char *start_pointer_to_default_value;
  char *end_pointer_to_default_value;
  start_pointer_to_default_value = strchr(default_led_config, '[');
  end_pointer_to_default_value = strchr(default_led_config, ']');
  int len_default_value_led_config = end_pointer_to_default_value - start_pointer_to_default_value - 1;
  strncpy(default_value_led_config, start_pointer_to_default_value + 1, len_default_value_led_config);
  default_value_led_config[len_default_value_led_config] = '\0';

  //TODO: save triggers data

  const char *led_trigger_default_on = "default-on";
  const char *led_trigger_blink = "timer";
  const char *led_blink_delay = "100";
  const int sleep_time = 100000;
  const int button_hold_guard_time_at_microsecond = button_hold_guard_time * 1000000; //convert seconds to microseconds;
  const int sleep_count = button_hold_guard_time_at_microsecond / sleep_time;
  fd_set exceptfds;
  int res;
  int i;
  char button_value;
  char *buf[MAX_BUF];
  pid_t exec_pid;
  int status;

  FD_ZERO(&exceptfds);
  FD_SET(fd_button_value, &exceptfds);
  read(fd_button_value, buf, MAX_BUF); // 'select()' won't work without this read - seems some problems in gpio driver

  writeLog("started demon");

  while (1) {
    writeLog("waiting for reset button is pressed");
    if (write(fd_led_trigger, default_value_led_config, strlen(default_value_led_config)) < 0) {
      writeLog("can't write %s, error: %s (%d)\n", path_to_led, strerror(errno), errno);
      exit(EXIT_FAILURE);
    }
    res = select(fd_button_value + 1, NULL, NULL, &exceptfds, NULL);
    if (write(fd_led_trigger, led_trigger_default_on, strlen(led_trigger_default_on)) < 0) {
      writeLog("can't write %s, error: %s (%d)\n", path_to_led, strerror(errno), errno);
      exit(EXIT_FAILURE);
    }
    if (res > 0 && FD_ISSET(fd_button_value, &exceptfds)) {
      for (i = 0; i < sleep_count; i++) {
        lseek(fd_button_value, 0, SEEK_SET);
        read(fd_button_value, &button_value, sizeof(char));
        if (button_value == BUTTON_PUSHED_VALUE) {
          usleep(sleep_time);
        } else {
          break;
        }
      }
      lseek(fd_button_value, 0, SEEK_SET);
      if (i != sleep_count) {
        writeLog("reset button hasn't been pressed for %d seconds", button_hold_guard_time);
      } else {
        writeLog("try to restore to default config");
        if (write(fd_led_trigger, led_trigger_blink, strlen(led_trigger_blink)) < 0) {
          writeLog("can't write %s, error: %s (%d)\n", path_to_led, strerror(errno), errno);
          exit(EXIT_FAILURE);
        }
        int fd_led_delay_on = open(path_to_led_delay_on, O_RDWR);
        if (fd_led_delay_on == -1) {
          writeLog("can't open %s, error: %s (%d)\n", path_to_led_delay_on, strerror(errno), errno);
          exit(EXIT_FAILURE);
        }
        if (write(fd_led_delay_on, led_blink_delay, strlen(led_blink_delay)) < 0) {
          writeLog("can't write %s, error: %s (%d)\n", path_to_led_delay_on, strerror(errno), errno);
          exit(EXIT_FAILURE);
        }
        close(fd_led_delay_on);
        int fd_led_delay_off = open(path_to_led_delay_off, O_RDWR);
        if (fd_led_delay_off == -1) {
          writeLog("can't open %s, error: %s (%d)\n", path_to_led_delay_off, strerror(errno), errno);
          exit(EXIT_FAILURE);
        }
        if (write(fd_led_delay_off, led_blink_delay, strlen(led_blink_delay)) < 0) {
          writeLog("can't write %s, error: %s (%d)\n", path_to_led_delay_off, strerror(errno), errno);
          exit(EXIT_FAILURE);
        }
        close(fd_led_delay_off);

        exec_pid = fork();
        if (exec_pid < 0) {
          writeLog("can't create a child process");
        } else if (exec_pid == 0) {
          writeLog("try to copy config");
          execl("/bin/bash", "bash", "-c", command_for_copy_config_files, NULL);
          writeLog("copy config failed");
          exit(EXIT_FAILURE);
        } else {
          waitpid(exec_pid, &status, 0);
          exec_pid = fork();
          if (exec_pid < 0) {
            writeLog("can't create a child process");
          } else if (exec_pid == 0) {
            writeLog("try to restart network");
            execl("/etc/init.d/networking", "networking", "restart", NULL);
            writeLog("restart network failed");
            exit(EXIT_FAILURE);
          } else {
            waitpid(exec_pid, &status, 0);
            exec_pid = fork();
            if (exec_pid < 0) {
              writeLog("can't create a child process");
            } else if (exec_pid == 0) {
              writeLog("try to change pass");
              execl("/usr/sbin/usermod", "usermod", "-p", default_root_password, "root", NULL);
              writeLog("change pass failed");
              exit(EXIT_FAILURE);
            } else {
              waitpid(exec_pid, &status, 0);
              exec_pid = fork();
              if (exec_pid < 0) {
                writeLog("can't create a child process");
              } else if (exec_pid == 0) {
                writeLog("try to reset iptables");
                execl("/bin/bash", "bash", "-c", "iptables-save && iptables-restore < /tmp/confrst.iptables.default", NULL);
                writeLog("reset iptables failed");
                exit(EXIT_FAILURE);
              } else {
                waitpid(exec_pid, &status, 0);
                sleep(5);
                writeLog("restored to default config");
              }
            }
          }
        }
      }
    }
  }
  exit(EXIT_SUCCESS);
}
