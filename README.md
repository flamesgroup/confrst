# confrst

Config reset daemon.

## Overview

**confrst** is a daemon to reset [GSMBOX](https://en.antrax.mobi/products/gsmgateway/) and [SIMBOX](https://en.antrax.mobi/products/simbox/) devices to its default configurations.
Daemon is triggered by push reset button for given number of seconds (default 5).

## Structure

**confrst** uses standard [Maven](https://maven.apache.org) project layout, with two additional directories:
* `configs` - directory contains default configuration files
* `run_script` - directory contains init.d script to run daemon on system start

## Build

### Build native code

To build native code for current platform (for now only ARM supported) next command must be executed in project directory:

```
$ mvn package -P native
```

### Build artifact

To build and install [jar](https://en.wikipedia.org/wiki/JAR_(file_format)) artifact with native bin, configs and run script as resources in project main directory execute next command:

```
$ mvn install
```

## Questions

If you have any questions about **confrst**, feel free to create issue with it.
